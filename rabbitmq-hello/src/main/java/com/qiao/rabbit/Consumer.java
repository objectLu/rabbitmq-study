package com.qiao.rabbit;

import com.qiao.util.ConnectionUtil;
import com.qiao.util.GetMessageUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/***
 * @author xiaojiaqiao
 * rabbitmq 消费者
 */
public class Consumer {


    public static void main(String[] args) throws Exception {

        Connection connection = ConnectionUtil.getConnection();


        Channel channel = connection.createChannel();

        GetMessageUtil.getMessage(channel,Producer.QUEUE_NAME);




        //不关闭资源，应该一直监听消息
        //channel.close();
        //connection.close();

    }


}
