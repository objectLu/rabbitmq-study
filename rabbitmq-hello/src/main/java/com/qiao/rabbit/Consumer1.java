package com.qiao.rabbit;

import com.qiao.util.ConnectionUtil;
import com.qiao.util.GetMessageUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @author xiaojiaqiao
 * @title: Consumer1
 * @projectName rabbitmq-study
 * @description: TODO
 * @date 2020/12/612:34
 */
public class Consumer1 {

    public static void main(String[] args) throws Exception {

        Connection connection = ConnectionUtil.getConnection();

        Channel channel = connection.createChannel();

        GetMessageUtil.getMessage(channel,Producer.QUEUE_NAME);

    }

}
