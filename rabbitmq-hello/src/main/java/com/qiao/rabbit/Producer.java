package com.qiao.rabbit;

import com.qiao.util.ConnectionUtil;
import com.qiao.util.SendMessageUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;


/***
 * @author xiaojiaqiao
 * @message : rabbitmq  生产者
 */
public class Producer {

    static final String QUEUE_NAME = "simple_queue";


    public static void main(String[] args) throws Exception {


        Connection connection = ConnectionUtil.getConnection();

        //   创建频道
        Channel channel = connection.createChannel();

        String message = "生产消息";

        for (int i = 0; i < 5; i++) {
            SendMessageUtil.sendMessage(channel,QUEUE_NAME,message+"-->"+i);
        }


        System.out.println("已经发送消息-->"+message);

        channel.close();

        connection.close();

    }


}
