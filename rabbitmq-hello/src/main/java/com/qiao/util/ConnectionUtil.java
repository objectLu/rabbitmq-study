package com.qiao.util;


import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.InputStream;
import java.util.Properties;

/***
 * @author xiaojiaqiao
 * 连接工具类
 */
public class ConnectionUtil {


    public static Connection getConnection() throws Exception {
        ConnectionFactory connectionFactory = new ConnectionFactory();

        Properties prop = new Properties();
        ClassLoader classLoader = ConnectionUtil.class.getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream("rabbitmq.properties");
        prop.load(resourceAsStream); /// 加载属性列表

        connectionFactory.setHost(prop.getProperty("Host"));

        connectionFactory.setPort(Integer.valueOf(prop.getProperty("Port")).intValue());

        connectionFactory.setVirtualHost(prop.getProperty("VirtualHost"));

        connectionFactory.setUsername(prop.getProperty("Username"));

        connectionFactory.setPassword(prop.getProperty("Password"));

        Connection connection = connectionFactory.newConnection();
        resourceAsStream.close();
        return connection;
    }


}
