package com.qiao.util;

import com.rabbitmq.client.Channel;

/**
 * @author xiaojiaqiao
 * @title: SendMessageUtil
 * @projectName rabbitmq-study
 * @description: TODO
 * @date 2020/12/612:24
 */
public class SendMessageUtil {

        public static void sendMessage(Channel channel, String queueName ,String message) throws Exception {

            channel.queueDeclare(queueName, true, false, false, null);


            channel.basicPublish("",queueName,null,message.getBytes());

        }



}
